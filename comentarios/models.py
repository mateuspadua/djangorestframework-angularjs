# -*- coding: utf-8 -*-

from django.db import models

class Comentario(models.Model):
	titulo = models.CharField("Título", max_length=255)
	comentario = models.TextField("Comentário")
